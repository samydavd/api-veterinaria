<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*Route::get('/', function () use ($router) {
    return $router->app->version();
});*/

Route::group(['as' => 'api'], function () {

	// PORTAL
	Route::group(['namespace' => 'Portal', 'as' => 'portal'], function () {

		// PERSONAS
		Route::group(['prefix' => 'personas', 'as' => 'personas'], function () {

			Route::post('/', [
				'uses' => 'PersonaController@index',
				'as' => 'index'
			]);

			Route::post('guardar', [
				'uses' => 'PersonaController@guardar',
				'as' => 'guardar'
			]);

			Route::post('actualizar', [
				'uses' => 'PersonaController@actualizar',
				'as' => 'actualizar'
			]);

			Route::post('eliminar', [
				'uses' => 'PersonaController@eliminar',
				'as' => 'eliminar'
			]);

			Route::post('mascotas', [
				'uses' => 'PersonaController@mascotas',
				'as' => 'mascotas'
			]);

		});

		// MASCOTAS
		Route::group(['prefix' => 'mascotas', 'as' => 'mascotas'], function () {
			
			Route::post('/', [
				'uses' => 'MascotaController@index',
				'as' => 'index'
			]);

			Route::post('guardar', [
				'uses' => 'MascotaController@guardar',
				'as' => 'guardar'
			]);

			Route::post('actualizar', [
				'uses' => 'MascotaController@actualizar',
				'as' => 'actualizar'
			]);

			Route::post('eliminar', [
				'uses' => 'MascotaController@eliminar',
				'as' => 'eliminar'
			]);
		});

		// RAZAS
		Route::group(['prefix' => 'razas', 'as' => 'razas'], function () {

			Route::post('/', [
				'uses' => 'RazaController@index',
				'as' => 'index'
			]);

			Route::post('guardar', [
				'uses' => 'RazaController@guardar',
				'as' => 'guardar'
			]);

			Route::post('actualizar', [
				'uses' => 'RazaController@actualizar',
				'as' => 'actualizar'
			]);
		});
	});
});