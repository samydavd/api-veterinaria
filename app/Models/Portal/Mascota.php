<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mascota extends Model
{
	use SoftDeletes;

	protected $table = 'mascotas';

	protected $fillable = [
		'nombre', 'fecha_nacimiento', 'raza_id', 'persona_id'
	];

	protected $hidden = [
		'deleted_at'
	];

	public function _persona ()
	{
		return $this->belongsTo(Persona::class, 'persona_id');
	}

	public function _raza ()
	{
		return $this->belongsTo(Raza::class, 'raza_id');
	}
}