<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Raza extends Model
{
	const UPDATED_AT = null;
	protected $table = 'razas';

	protected $fillable = [
		'nombre'
	];

	public function _mascotas ()
	{
		return $this->hasMany(Mascota::class, 'raza_id');
	}
}