<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
	use SoftDeletes;

	protected $table = 'personas';

	protected $fillable = [
		'nombre', 'apellido', 'correo'
	];

	protected $appends = [
		'nombre_completo'
	];

	protected $hidden = [
		'deleted_at'
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($value) {
            Mascota::where('persona_id', $value->id)->delete();
        });
	}

	public function _mascotas ()
	{
		return $this->hasMany(Mascota::class, 'persona_id');
	}

	public function getNombreCompletoAttribute ()
	{
		return "$this->nombre $this->apellido";
	}
}