<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Portal\Mascota;
use App\Models\Portal\Persona;
use App\Models\Portal\Raza;

class MascotaController extends Controller
{
	public function index()
	{
		$mascotas = Mascota::orderByDesc('id')
		->with('_persona', '_raza')
		->get();

		$personas = Persona::orderBy('nombre')->get();
		$razas = Raza::orderBy('nombre')->get();

		return response([
			'mascotas' => $mascotas,
			'personas' => $personas,
			'razas' => $razas
		], 201);
	}

	public function guardar(Request $request)
	{
		$this->validate($request, [
			'nombre' => 'required|string',
			'fecha_nacimiento' => 'required|date',
			'raza_id' => 'required|integer|exists:razas,id',
			'persona_id' => 'required|integer|exists:personas,id'
		]);

		try {

			$mascota = new Mascota;
			$mascota->fill($request->all());
			$mascota->save();

			$success = 'Mascota guardada exitosamente';

			$mascota->load('_persona', '_raza');

			return response([
				'mascota' => $mascota,
				'success' => $success
			], 201);
		}
		catch(\Exception $e) {

			return response (['error' => $e->getMessage()], 500);

		}
	}

	public function actualizar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|integer|exists:mascotas,id',
			'nombre' => 'required|string',
			'fecha_nacimiento' => 'required|date',
			'raza_id' => 'required|integer|exists:razas,id',
			'persona_id' => 'required|integer|exists:personas,id'
		]);

		try {

			$mascota = Mascota::find($request->id);
			$mascota->fill($request->all());
			$mascota->save();

			$success = 'Mascota actualizada exitosamente';

			$mascota->load('_persona', '_raza');

			return response ([
				'mascota' => $mascota,
				'success' => $success
			], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}

	public function eliminar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|integer|exists:mascotas,id'
		]);

		try {

			$mascota = Mascota::find($request->id);
			$mascota->delete();

			$success = 'Mascota eliminada exitosamente';

			return response (['success' => $success], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}
}