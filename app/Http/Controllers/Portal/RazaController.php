<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Portal\Raza;

class RazaController extends Controller
{
	public function index()
	{
		$razas = Raza::orderByDesc('id')->get();

		return response(['razas' => $razas], 201);
	}

	public function guardar(Request $request)
	{
		$this->validate($request, [
			'nombre' => 'required|string||unique:razas,nombre',
		]);

		try {

			$raza = new Raza;
			$raza->fill($request->all());
			$raza->save();

			$success = 'Raza almacenada exitosamente';

			return response ([
				'raza' => $raza,
				'success' => $success
			], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}

	public function actualizar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|integer|exists:razas,id',
			'nombre' => 'required|string|unique:razas,nombre,$request->id'
		]);

		try {

			$raza = Raza::find($request->id);
			$raza->fill($request->all());
			$raza->save();

			$success = 'Raza actualizada exitosamente';

			return response ([
				'raza' => $raza,
				'success' => $success
			], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}
}