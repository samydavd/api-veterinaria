<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Portal\Persona;
use App\Models\Portal\Raza;

class PersonaController extends Controller
{
	public function index()
	{
		$personas = Persona::orderByDesc('id')->get();

		return response(['personas' => $personas], 201);
	}

	public function crear()
	{
		$razas = Raza::get();

		return response(['razas' => $razas], 201);
	}

	public function guardar(Request $request)
	{
		$this->validate($request, [
			'nombre' => 'required|string',
			'apellido' => 'required|string',
			'correo' => 'required|string|email:dns|unique:personas'
		]);

		try {

			$persona = new Persona;
			$persona->fill($request->all());
			$persona->save();

			$success = 'Persona almacenada exitosamente';

			return response ([
				'persona' => $persona,
				'success' => $success
			], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}

	public function actualizar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|integer|exists:personas,id',
			'nombre' => 'required|string',
			'apellido' => 'required|string',
			'correo' => "required|string|email:dns|unique:personas,correo,$request->id"
		]);

		try {

			$persona = Persona::find($request->id);
			$persona->fill($request->all());
			$persona->save();

			$success = 'Persona actualizada exitosamente';

			return response ([
				'persona' => $persona,
				'success' => $success
			], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}

	public function eliminar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|integer|exists:personas,id'
		]);

		try {

			$persona = Persona::find($request->id);
			$persona->delete();

			$success = 'Persona y mascotas asociadas eliminados exitosamente';

			return response (['success' => $success], 201);
		}
		catch(\Exception $e) {

			return response(['error' => $e->getMessage()], 500);

		}
	}

	public function mascotas ()
	{
		$personas = Persona::with('_mascotas._raza')->get();

		return response(['personas' => $personas], 201);
	}
}