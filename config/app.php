<?php

return [
	'locale' => 'es',
	'fallback_locale' => 'en',
	'providers' => [
		Laraveles\Spanish\SpanishServiceProvider::class
	]
];